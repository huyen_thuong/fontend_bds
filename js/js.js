

// slider
;
(function($, win) {
    "use strict";

    //Documents ready
    $(function() {
        slider();
        slider_main();
        slider_content();
    });

    function slider_content() {
        $('.slider_top').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            autoplay: true,
            asNavFor: '.slider_bottom'
        });
        $('.slider_bottom').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider_top',
            dots: true,
            autoplay: true,
            centerMode: true,
            focusOnSelect: true
        });
    }








    function slider() {
        $('.slider_content').slick({
            variableWidth: true,
            autoplay: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '.prev',
            nextArrow: '.next',

        });
    }

    function slider_main() {
        $('.slider_main_visual').slick({
            variableWidth: true,
            autoplay: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 1000,
            autoplaySpeed: 2000,
            centerMode: true,
            prevArrow: '.btn_prev',
            nextArrow: '.btn_next',
            dots: true,
            customPaging: function(slider_main_visual, i) {
                i++
                return '<a>' + i + '</a>';
            },
        });
        $('.slick-dots').appendTo('.paging .inner');

    }




}(jQuery, window));



// slider slick
$('.portfolio-item-slider').on('init', function(event, slick, currentSlide){
  var nrCurrentSlide = slick.currentSlide + 1, // din cauza ca e array si incepe de la 0
      totalSlidesPerPage = nrCurrentSlide + 3; // daca ai 5 thumb-uri pe pagina pui + 4
  $('.controls').html(nrCurrentSlide + " - " + totalSlidesPerPage + " of " + slick.slideCount);
});

$('.portfolio-thumb-slider').slick({
  slidesToShow: 7,
  slidesToScroll: 1,
  asNavFor: '.portfolio-item-slider',
  dots: false,
  arrows: false,
  focusOnSelect: true,
  autoplay: true,
  infinite: false
});

$('.portfolio-item-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  arrows: false,
  asNavFor: '.portfolio-thumb-slider',
  infinite: false
});

var current = 0; // current slider dupa refresh
$('.portfolio-thumb-slider .slick-slide:not(.slick-cloned)').eq(current).addClass('slick-current');
$('.portfolio-item-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
  current = currentSlide;
  $('.portfolio-thumb-slider .slick-slide').removeClass('slick-current');
  $('.portfolio-thumb-slider .slick-slide:not(.slick-cloned)').eq(current).addClass('slick-current');
  var nrCurrentSlide = slick.currentSlide + 1, // din cauza ca e array si incepe de la 0
      totalSlidesPerPage = nrCurrentSlide + 3; // daca ai 5 thumb-uri pe pagina pui + 4

  if(totalSlidesPerPage > slick.slideCount) {
    $('.controls').html(nrCurrentSlide + " - " + slick.slideCount + " of " + slick.slideCount);
  } else {
    $('.controls').html(nrCurrentSlide + " - " + totalSlidesPerPage + " of " + slick.slideCount);
  }
});